import React from 'react';

import { Route, Routes } from 'react-router-dom';

import HomePage from './pages/homePage';
import Meal from './pages/meal';
import Page404 from './pages/page404';
import Pasta from './pages/pasta';
import Appetizer from './pages/appetizer';
import Desserts from './pages/desserts';
import Dips from './pages/dips';

export default function Router() {

    return(
        <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/appetizer" element={<Appetizer/>} />
            <Route path="/desserts" element={<Desserts/>} />
            <Route path="/dips" element={<Dips />} />
            <Route path="/meal" element={<Meal/>} />
            <Route path="/pasta" element={<Pasta/>} />
            <Route path="/404" element={<Page404 />} />
            <Route path="*" element={<Page404 />} />
        </Routes>
    )
}

