import { Box, Container, Typography } from '@mui/material'
import React from 'react'
import AppBarComponent from '../componets/appBarComponent'
import { CardMenuAppetizer, CardMenuDesserts, CardMenuDips, CardMenuMeal, CardMenuPasta } from '../componets/CardMenu'

const HomePage = () => {
    return (
        <div style={styleDiv}> 
            <AppBarComponent name='Inicio'/>
                <Container fixed>
                    <Typography variant="h5" >
                        Recetas de cocina
                    </Typography>
                <Box sx={{ display: 'flex', flexWrap: 'wrap'}}>
                    <Box sx={{ m: 2}}>
                        <CardMenuAppetizer />
                    </Box>
                    <Box sx={{ m: 2}}>
                        <CardMenuMeal />
                    </Box>
                    <Box sx={{ m: 2}}>
                        <CardMenuPasta />
                    </Box>
                    <Box sx={{ m: 2}}>
                        <CardMenuDips />
                    </Box>
                    <Box sx={{ m: 2}}>
                        <CardMenuDesserts />
                    </Box>
                </Box>
            </Container>
        </div>
    )
}

const styleDiv ={
    width:'100%',

}

export default HomePage