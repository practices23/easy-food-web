import React from 'react'
import { Link as RouterLink } from 'react-router-dom';
import {Button, Typography } from '@mui/material'

const Page404 = () => {
    return (
        <div style={stylesComponents.styleDiv}>
            <Typography align='center' variant='h4' >
                Lo sentimos, página no encontrada
            </Typography>
            <Typography align='center'>
                Lo sentimos, no pudimos encontrar la página que estás buscando. ¿Quizás has escrito mal la URL? Asegúrese de revisar su ortografía.
            </Typography>
            <Typography align='center' variant='h1' >
                404
            </Typography>
            <div style={{textAlign:'center'}}>
                <Button style={stylesComponents.styleButton} variant='contained' to="/" component={RouterLink}>
                    Regresar al inicio
                </Button>
            </div>
        

        </div>
    )
}

export default Page404

const stylesComponents = {
    styleDiv:{
        width:'60%',
        padding:'50px',
        backgroundColor: '#ecf0f1',
        borderCollapse: 'collapse',
        margin: 'auto'
    },
    styleButton:{
        color:'#fff',
        backgroundColor:'#ae8fba',
    }
}
