import { Box, Button, Card, CardContent, CardHeader, Container, List, ListItem, ListItemButton, ListItemText, Modal } from '@mui/material'
import React, { useState } from 'react'
import AppBarComponent from '../componets/appBarComponent'
import { Link as RouterLink } from 'react-router-dom'; 
import CardReceta from '../componets/cardReceta';

const mealList =[
    {
        name:'Lasaña',
        description:'Ideal para comidas especiales',
        ingredientes:["Pasta de lasaña",
            "Salsa boloñesa",  
            "2 Calabacitas", 
            "2 Zanahoria", 
            "500 grs Carne molida", 
            "400 grs Queso manchego", 
            ],
        preparacion:["Freír a término medio la carne, con la zanahoria y calabaciras en cubitos",
            "Hacer capaz iniciando con la pasta, después los ingredientes",
            "Cocinar al horno a 200°C durante 20 minutos"
            ],
        src:'https://lh3.googleusercontent.com/u/0/d/1SdOo72f8TUaBWDdoOjLa_ws5iFdStVc1=w1231-h977-iv1'
    },
]

const Pasta = () => {

    const [open, setOpen] = useState(false);
    const [preparation, setPreparation] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [title, setTitle] = useState('');
    const [src, setSrc] = useState('');

    const handleOpen = (receta) => {
        setTitle(receta[0])
        setIngredients(receta[1])
        setPreparation(receta[2])
        setSrc(receta[3])
        setOpen(true);
    } 
    const handleClose = () => setOpen(false);

    return (
        <div style={styleDiv}> 
            <AppBarComponent name='Pasta'/>
            <Container fixed>
                <Card>
                    <CardHeader action={<Button component={RouterLink} to='/'> Atras </Button>}/>
                    <CardContent>
                        {mealList.map(index =>
                            <List>
                                <ListItemButton onClick={()=>handleOpen([index.name,index.ingredientes, index.preparacion, index.src])}>
                                    <ListItem>
                                        <ListItemText primary={index.name} secondary={index.description}  />
                                    </ListItem>
                                </ListItemButton>
                            </List>
                        )}
                    </CardContent>
                </Card>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={styleModal}>
                        <CardReceta ingredientes={ingredients} preparacion={preparation} name={title} imge={src} />
                    </Box>
                </Modal>
            </Container>
        </div>
    )   
}

const styleDiv ={
    width:'100%',
}

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default Pasta