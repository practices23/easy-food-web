import { Box, Button, Card, CardContent, CardHeader, Container, List, ListItem, ListItemButton, ListItemText, Modal } from '@mui/material'
import React, { useState } from 'react'
import AppBarComponent from '../componets/appBarComponent'
import { Link as RouterLink } from 'react-router-dom'; 
import CardReceta from '../componets/cardReceta';

const dipsList =[
    {
        name:'Salsa Verde',
        description:'Ideal para la carnita asada',
        ingredientes:["chiles jalapeños",
            "cebolla",
            "ajo",
            "Sal al gusto",
            ],
        preparacion:["Freír unos 6 chiles con cebolla y 2 ajos",
            "Licuar los ingredientes con el aceite y un poco de agua",
            "Agregar un sal al gusto"
            ],
        src:''
    },
]

const Dips = () => {

    const [open, setOpen] = useState(false);
    const [preparation, setPreparation] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [src, setSrc] = useState('');
    const [title, setTitle] = useState('');

    const handleOpen = (receta) => {
        setTitle(receta[0])
        setIngredients(receta[1])
        setPreparation(receta[2])
        setSrc(receta[3])
        setOpen(true);
    } 
    const handleClose = () => setOpen(false);

    return (
        <div style={styleDiv}> 
            <AppBarComponent name='Salsas'/>
            <Container fixed>
                <Card>
                    <CardHeader action={<Button component={RouterLink} to='/'> Atras </Button>}/>
                    <CardContent>
                        {dipsList.map(index =>
                            <List>
                                <ListItemButton onClick={()=>handleOpen([index.name,index.ingredientes, index.preparacion, index.src])}>
                                    <ListItem>
                                        <ListItemText primary={index.name} secondary={index.description}  />
                                    </ListItem>
                                </ListItemButton>
                            </List>
                        )}
                    </CardContent>
                </Card>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={styleModal}>
                        <CardReceta ingredientes={ingredients} preparacion={preparation} name={title} imge={src} />
                    </Box>
                </Modal>
            </Container>
        </div>
)
}

const styleDiv ={
width:'100%',

}

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default Dips