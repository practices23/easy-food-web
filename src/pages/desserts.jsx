import { Box, Button, Card, CardContent, CardHeader, Container, List, ListItem, ListItemButton, ListItemText, Modal } from '@mui/material'
import React, { useState } from 'react'
import AppBarComponent from '../componets/appBarComponent'
import { Link as RouterLink } from 'react-router-dom'; 
import CardReceta from '../componets/cardReceta';

const dessertsList =[
    {
        name:'Flan Napolitano',
        description:'un postre para compartir',
        ingredientes:["4 huevos",
            "1 lata de leche clavel", 
            "1 lata de lechera",
            "Un chorro de vainilla", 
            "Medio queso crema", 
            ],
        preparacion:["En el molde, derretir la azúcar en el fuego y repartir",
            "Licuar los ingredientes",
            "Integrar los ingredientes en el molde",
            "En una olla de presión cocinar a baño maría durante 20 minutos"
            ],
        src:''
    },
    {
        name:'Pastel imposible',
        description:'Flan con chocolate',
        ingredientes:[],
        preparacion:[],
        src:'https://lh3.googleusercontent.com/u/0/d/14YOzlDpEEbxxr1YeKzf4Rj1Kb8MTLw4Y=w200-h190-p-k-nu-iv1'
    },
]

const Desserts = () => {

    const [open, setOpen] = useState(false);
    const [preparation, setPreparation] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [title, setTitle] = useState('');
    const [src, setSrc] = useState('');

    const handleOpen = (receta) => {
        setTitle(receta[0])
        setIngredients(receta[1])
        setPreparation(receta[2])
        setSrc(receta[3])
        setOpen(true);
    } 
    const handleClose = () => setOpen(false);

    return (
        <div style={styleDiv}> 
            <AppBarComponent name='Postres'/>

            <Container fixed>
                <Card>
                    <CardHeader action={<Button component={RouterLink} to='/'> Atras </Button>}/>
                    <CardContent>
                        {dessertsList.map(index =>
                            <List>
                                <ListItemButton onClick={()=>handleOpen([index.name,index.ingredientes, index.preparacion, index.src])}>
                                    <ListItem>
                                        <ListItemText primary={index.name} secondary={index.description}  />
                                    </ListItem>
                                </ListItemButton>
                            </List>
                        )}
                    </CardContent>
                </Card>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={styleModal}>
                        <CardReceta ingredientes={ingredients} preparacion={preparation} name={title} imge={src} />
                    </Box>
                </Modal>

            </Container>

        </div>
)
}

const styleDiv ={
    width:'100%',
}
const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default Desserts