import { Box, Button, Card, CardContent, CardHeader, Container, List, ListItem, ListItemButton, ListItemText, Modal } from '@mui/material'
import React, { useState } from 'react'
import AppBarComponent from '../componets/appBarComponent'
import { Link as RouterLink } from 'react-router-dom'; 
import CardReceta from '../componets/cardReceta';

const appetizerList =[
    {
        name:'Empanadas',
        description:'Antojitos jarochos preparados de masa',
        ingredientes:["Masa de maíz", "carne molida", "jitomate","cebolla" ],
        preparacion: ["Preparar la carne molida a la mexicana (se puede agregar chile al gusto)",
            "amasar la masa de maíz",
            "realizar bolitas de maíz y aplanar con una prensa (puede ser con la mano)",
            "rellenar con el picadillo y cerrar a la mitad",
            "aplastar la orilla para que no se abra",
            "freír en aceite caliente"
            ],
        src:''
    },
    {
        name:'Tacos dorados',
        description:'Tortilla de maiz, con un toque especial',
        ingredientes:[],
        preparacion:[],
        src:''
    },
    {
        name:'Niño envuelto',
        description:'Preparado con pan blanco ideal para compartir',
        ingredientes:["Pan blanco", "Queso Crema","Chipotle", "10 rebanadas de Jamon","8 rebanadas de Queso amarillo"],
        preparacion:[
            "Quitar las orillas del pan blanco",
            "Se realiza una capa de pan blanco y se aplana con un rodillo",
            "se licua el queso crema con chipolte al gusto",
            "se corta el queso amarillo y jamon en cuadros",
            "se pone la salsa que licuamos en el pan y despues ponemos el jamon y queso amarillos y lo distribuimos por el pan",
            "doblamos el pan en forma de taco"
        ],
        src:'https://lh3.googleusercontent.com/fife/ALs6j_EntIu8P4QVHxw8WfBUzYxJwAx5hAQqvQwFQ2cYYIr2FMZTvbD-lGR7eXwRPK2KUDgwbVpJAYIdE31_7COxwfJ-MSAMPTLq9ayLSyKquASAFKP5t0AG6hmQF6xQsDxyge47kOpDvVJ8skF9r0kiWWd7gGYevU4kbaFkqzgCqgCBHjHCV-gf9s3esr3LxFF6WLLmMZti5ylMzkIr5lHpa0qEVuZnQAT33JOpBu5Jk6E2m3bhgW1h9FfYVxEoNp_wUOoyuRu9KKoD3J7-jODl3lYq6LcvOPq8ZQhTzXtsRU4feHFBXj9-Nhgt2wvYXfkB3Z0tCanW9cIolNlFmZa85YpyesqM1qqmsVyaDwwDmfy8-2Za_W47QspnH0yCL9Or-HfC_eAdaxqJCn3190EQcI5BtY1xZurb6Txv-nxdtq6q2RSTOdrRh-cvqgnmZelsooscxoTosCLKWILyRzLykMrSA78E9b5MzoMJB4Kbrxsdimto1VsAkV_AIw4AkDipKGHlN2YwnyJTbpoFNm3e_D5hmiAfpD73UkFZ8zUKTKm2pyzyvVPYJnFB7G0vHxBwurm6bgxtaFRX2jhG56TzwHyi8XYfHtYQB8p09F9b6zGd3oLBqoG9bpefOGpPlktXTIAnYQSrDjW664WRmdeNaKjv7DeU5UxHwDb15HtWQAhp1jHuZmpeJkRpQJcSCsXGUPnWY1G01p66P6nMPNjUye1Hfzh3gKWvhp2Zm5_P_xuf6aRab_A3DAVuJ14YaCr43S_EP-h10SHh1JTJopbkxz1zM55IDsZPQEzQroYCWk_SODdE39_oGRTaYTn8iwBbJHr5xyiDlXW2_KnnP1YsmB7427_SfR9AdANDc2BifemkUmIK83fcJhnSd2CivhJpEFAn-SSIGgho3Gs6h8sArOb_1Qxy2e0GoCX8DJSkhL7Ad4S0pSmqwSlVq2-oVSkvk-nLFdvs10z9ZEVMNSVEMdnY-F11w-t0yUN2TOksY2Ei9Sm2Cjp53JsI-EplaJ-slCDD53wAOFr2D9VUYnmNGKU01_LTDv672trTHWP1Gji0mat_vV3UYG_W1NCMSWPv5Xz_nyEXXNEXVwbWSjOYXwEAajfyvs8355IQCFSBaPv0l_wLhc-6bQiPqMjwP15qbRx885FWXgNcaWY5D98xZJuzH5g3mk3RAXAYVKEJX-xovei-UCdsv_bNhKCihP_ibpsQtXwppywQXfm9PW74eM8X8m6lVzyqzQTPsDOSre2G-cH7RADzL-QcWy5Ld3fzncjImvKpELT06wsqsudo_uNYTHve7V1UZ7QFrXaxcQYDF_jeM3qYvBxe7CW7m91emlZ73btTGcnjB2GiuxECeJ-3u0g5uQOEZBCqPBCLMcIqQqYFcUzKd4hUMph149N17aVygUZkT3RQrVlhbFwMKPOuUoYKKGh1bb9PndUTPBo_TLf_regBZMnaax1ZRSIbtRpQHfuasAfIM2F0hQxf1DGUfMIVO4CMLklXLwUr0dzYVd1wzVYIr5ynOaZk7S1sRLvZ7QkFItISekE5xW1DpufB3TOL0IV04lq0JdG7NSGJfSQqkkYDolYGzYtU8haLij1H-QWJG1_v=w1256-h962'
    }
]


const Appetizer = () => {
    
    const [open, setOpen] = useState(false);
    const [preparation, setPreparation] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [title, setTitle] = useState('');
    const [src, setSrc] = useState('');

    const handleOpen = (receta) => {
        setTitle(receta[0])
        setIngredients(receta[1])
        setPreparation(receta[2])
        setSrc(receta[3])
        setOpen(true);
    } 
    const handleClose = () => setOpen(false);

    return (
        <div style={styleDiv}> 
            <AppBarComponent name='Inicio'/>
            <Container fixed>
                <Card>
                    <CardHeader action={<Button component={RouterLink} to='/'> Atras </Button>}/>
                    <CardContent>
                        {appetizerList.map(index =>
                            <List>
                                <ListItemButton onClick={()=>handleOpen([index.name,index.ingredientes, index.preparacion, index.src])}>
                                    <ListItem>
                                        <ListItemText primary={index.name} secondary={index.description}  />
                                    </ListItem>
                                </ListItemButton>
                            </List>
                        )}
                    </CardContent>
                </Card>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={styleModal}>
                        <CardReceta ingredientes={ingredients} preparacion={preparation} name={title} imge={src}/>
                    </Box>
                </Modal>
                
            </Container>
        </div>
    )
}

const styleDiv ={
    width:'100%',

}
const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default Appetizer