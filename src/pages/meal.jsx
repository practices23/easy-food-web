import { Box, Button, Card, CardContent, CardHeader, Container, List, ListItem, ListItemButton, ListItemText, Modal } from '@mui/material'
import React, { useState } from 'react'
import AppBarComponent from '../componets/appBarComponent'
import { Link as RouterLink } from 'react-router-dom'; 
import CardReceta from '../componets/cardReceta';

const mealList =[
    {
        name:'Arroz',
        description:'Arroz blanco para acompañar a tus comidas',
        ingredientes:["1 taza de arroz",
                        "2 tazas de agua",
                        "un cubito de caldo de pollo",
                        "un diente de ajo",
                        "un pedazo de cebolla"
                    ],
        preparacion:[   
                        "limpiar el arroz y cortar el ajo y cebolla en cubitos pequeños",
                        "freir el arroz con el ajo y cebolla",
                        "una vez frito el arroz, agregar el agua y caldo de pollo",
                        "esperar a que se consuma el agua a fuego bajito"
                    ],
        src:'https://lh3.googleusercontent.com/u/0/d/11q8EyCRxm0gV6fbT8IoWkfdhSm4qQoMv=w200-h190-p-k-nu-iv1'
    },
    {
        name:'Barbacoa',
        description:'Carne de pollo o res, en rica salsa',
        ingredientes:["5 chiles guajillo", 
            "2 jitomates",
            "Un puño de chile seco",
            "Carne de res o pollo",
            "Hojas de aguacatillo", 
            ],
        preparacion:["Hervir los jitomates, chile guajillo y chile seco",
            "Licuar los ingredientes hervidos con ajo, cebolla y un poquito de comino",
            "En olla de presión freír lo que licuamos, y se le agrega sal",
            "Agregamos la carne con las hojas de aguacatillo y se tapa" 
            ],
        src:''
    },
    {
        name:'Tesmole',
        description:'Carne de pollo o res, en rica salsa',
        ingredientes:["5 chiles guajillo", 
            "2 jitomates",
            "Un puño de chile seco",
            "Carne de res o pollo",
            "Hojas de acullo o hierva santa",
            "medio kilo de masa" 
            ],
        preparacion:[
            "Hervir el pollo en una caserola",
            "Hervir los jitomates, chile guajillo y chile seco",
            "Licuar los ingredientes hervidos con ajo, cebolla y un poquito de comino",
            "agregamos la salsa a un recipiente y se le agrega sal",
            "realizamos una bolitas de masa, para ello agarramos un poco de aceite para que la masa no se pegue",
            "Agregamos la carne con el caldito,las bolitas de masa y las hojas de acullo, y se tapa" 
            ],
        src:''
    },
    {
        name:'Tortitas de atun',
        description:'Para acompañar en la cuaresma o disfrutar el atun de manera diferente',
        ingredientes:["Pan molido", 
            "Una clara de huevo", 
            "Una papa mediana",
            "Atún" 
            ],
        preparacion:[ "Hervir la papa", 
            "Aplastar la papa en forma de pure",
            "Mezclar los ingredientes y le echamos sal",
            "Hacemos las bolitas y las ponemos a freír"
            ],
        src:''
    },
    {
        name:'Chiles jalapeños con queso crema',
        description:'ideal para acompañar',
        ingredientes:[
                        "chiles jalapeños",
                        "queso crema",
                        "tocino"
                    ],
        preparacion:[   
                        "lavar y desvenar los chiles jalapeños",
                        "rellenar con queso crema",
                        "envolver los chiles con tocino",
                        "meter en el horno 200°C durante 30-45 minutos o en asador"
                    ],
        src:'https://lh3.googleusercontent.com/u/0/d/1zQKv1rfn48aEXDJ8XefWtyT3jkRFIbzb=w200-h190-p-k-nu-iv1'
    }
]


const Meal = () => {

    const [open, setOpen] = useState(false);
    const [preparation, setPreparation] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [title, setTitle] = useState('');
    const [src, setSrc] = useState('');

    const handleOpen = (receta) => {
        setTitle(receta[0])
        setIngredients(receta[1])
        setPreparation(receta[2])
        setSrc(receta[3])
        setOpen(true);
    } 
    const handleClose = () => setOpen(false);

    return (
        <div style={styleDiv}> 
            <AppBarComponent name='Comida'/>
            <Container fixed>
                <Card>
                    <CardHeader action={<Button component={RouterLink} to='/'> Atras </Button>}/>
                    <CardContent>
                        {mealList.map(index =>
                            <List>
                                <ListItemButton onClick={()=>handleOpen([index.name,index.ingredientes, index.preparacion, index.src])}>
                                    <ListItem>
                                        <ListItemText primary={index.name} secondary={index.description}  />
                                    </ListItem>
                                </ListItemButton>
                            </List>
                        )}
                    </CardContent>
                </Card>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={styleModal}>
                        <CardReceta ingredientes={ingredients} preparacion={preparation} name={title} imge={src} />
                    </Box>
                </Modal>
            </Container>
        </div>
)
}

const styleDiv ={
    width:'100%',
}

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default Meal