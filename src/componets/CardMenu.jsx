import { Button, Card, styled, Typography } from '@mui/material';
import React from 'react'
import { Fastfood, LocalCafe, RamenDining, RestaurantMenu, Whatshot } from '@mui/icons-material/';
import { Link as RouterLink } from 'react-router-dom'; 

const RootStyle = styled(Card)(({ theme }) => ({
    textAlign: 'center',
    padding: theme.spacing(5, 0),
    marginBottom: 20,
    color: "#4c5e91",
    boxShadow: '#ffff',
}));

const IconWrapperStyle = styled('div')(({ theme }) => ({
    margin: 'auto',
    display: 'flex',
    borderRadius: '50%',
    alignItems: 'center',
    width: theme.spacing(25),
    height: theme.spacing(15),
    justifyContent: 'center',
    marginBottom: theme.spacing(3),
    color: theme.palette.primary.main,
    
}));


export const CardMenuAppetizer = () => {
    return (
        <Button component={RouterLink} to='/appetizer'>
            <RootStyle>
                <IconWrapperStyle>
                    <Fastfood style={styleIcon} sx={{ fontSize: 100 }} />
                </IconWrapperStyle>
                <Typography variant="subtitle2" sx={{ opacity: 0.72 }}>
                    Aperitivos
                </Typography>
            </RootStyle>
        </Button>
    )
}

export const CardMenuDesserts = () => {
    return (
        <Button component={RouterLink} to='/desserts'>
            <RootStyle>
                <IconWrapperStyle>
                    <LocalCafe style={styleIcon} sx={{ fontSize: 100 }} />
                </IconWrapperStyle>
                <Typography variant="subtitle2" sx={{ opacity: 0.72 }}>
                    Postres
                </Typography>
            </RootStyle>
        </Button>
    )
}

export const CardMenuDips = () => {
    return (
        <Button component={RouterLink} to='/dips'>
            <RootStyle>
                <IconWrapperStyle>
                    <Whatshot style={styleIcon} sx={{ fontSize: 100 }} />
                </IconWrapperStyle>
                <Typography variant="subtitle2" sx={{ opacity: 0.72 }}>
                    Salsas
                </Typography>
            </RootStyle>
        </Button>
    )
}

export const CardMenuMeal = () => {
    return (
        <Button component={RouterLink} to='/meal'>
            <RootStyle>
                <IconWrapperStyle>
                    <RestaurantMenu style={styleIcon} sx={{ fontSize: 100 }} />
                </IconWrapperStyle>
                <Typography variant="subtitle2" sx={{ opacity: 0.72 }}>
                    Comida
                </Typography>
            </RootStyle>
        </Button>
    )
}

export const CardMenuPasta = () => {
    return (
        <Button component={RouterLink} to='/pasta'>
            <RootStyle>
                <IconWrapperStyle>
                    <RamenDining style={styleIcon} sx={{ fontSize: 100 }} />
                </IconWrapperStyle>
                <Typography variant="subtitle2" sx={{ opacity: 0.72 }}>
                    Pasta
                </Typography>
            </RootStyle>
        </Button>
    )
}

export const CardList = ({name}) => {
    return (
        <Button component={RouterLink} to='/pasta'>
            <RootStyle>
                <Typography variant="subtitle2" sx={{ opacity: 0.72 }}>
                    {name}
                </Typography>
            </RootStyle>
        </Button>
    )
}

const styleIcon = {
    color:'#4c5e91'
}