import { Card, CardContent, CardHeader, CardMedia, Typography } from '@mui/material'
import React from 'react'

const CardReceta = ({name, ingredientes, preparacion, imge}) => { 
    return (
        <Card>
            <CardHeader title={name}/>
            {imge==='' ? null :
                <CardMedia
                    sx={{ height: 200, width:250 }}
                    component="img"
                    image={imge}
                    alt="Image"
                />
            }
            <CardContent> 
                <Typography>Ingredientes</Typography>
                <ul>
                    {ingredientes.length ===0 ? null :  ingredientes.map(index =>
                        <li>{index}</li>
                    )}
                </ul>

                <Typography>Preparacion</Typography>
                <ol>
                    {preparacion.length ===0 ? null : preparacion.map(index =>
                        <li>{index}</li>
                    )}
                </ol>
            </CardContent>
        </Card>
    )
}

export default CardReceta