import { AppBar, Box, Toolbar, Typography } from '@mui/material'
import React from 'react'

/**
 * 
 * @param {*} name nombre para el app bar  
 * @returns 
 */
const AppBarComponent = (props) => {
    
    return (
        <Box style={styleBox}>
            <AppBar style={{background:'#473469'}} position="static">
                <Toolbar variant="dense">
                    <Typography variant="h6" color="inherit" component="div">
                        {props.name}
                    </Typography>
                </Toolbar>
            </AppBar>
        </Box>
    )
}

const styleBox ={
    width:'100%',
}

export default AppBarComponent


